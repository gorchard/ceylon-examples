# Ceylon examples

**Small dummy project containing Ceylon code.** 


--------------------


This repository is used to test the language [Ceylon][cylon], it's syntax and capabilities. 



[ceylon]: http://ceylon-lang.org/ "Ceylon"