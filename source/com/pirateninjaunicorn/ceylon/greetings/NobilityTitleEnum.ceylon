// this is supposed to replace enums :/ 
// hm, let's see what we can do with that

shared abstract class NobilityTitleEnum(shared String title)
        of baron | lord | earl | duke {

	shared formal String address();
	
	shared actual String string => title;
}
 
shared object duke extends NobilityTitleEnum("Duke") {
	shared actual String address() => "Your Grace";
}
shared object earl extends NobilityTitleEnum("Earl") {
	shared actual String address() => "Your Lordship";
}
shared object baron extends NobilityTitleEnum("Baron") {
	shared actual String address() => "Your Lordship";
}
shared object lord extends NobilityTitleEnum("Lord") {
	shared actual String address() => "Sir";
}

doc("So, finally some part I massively dislike:
     It definitely belongs to the enum, but I can't put it in there :(
     And furthermore: switch/case, isn't there something better?")
shared NobilityTitleEnum|Null nobilityTitleEnumFromValue(String title) {
	switch(title)
	case("Lord") {
		return duke;
	}
	case("Duke") {
		return duke;
	}
	case("Earl") {
		return duke;
	}
	case("Baron") {
		return duke;
	} else {
		return null;
	}
}