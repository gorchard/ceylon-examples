
"Superfluous abstract class ... just learning how to use"
shared abstract class Greeting()
	of Courtesy|Ignorance { // this way nothing else is allowed to inherit directly from Greeting

	shared formal void printInvitation();
	
	// abstract method
	doc("Creates proper greeting")
	shared formal String greet();
	
	// a default implementation would be possible, too
	/*
	shared default String greet() {
		return "Hi there";
	}
	 */
}