
shared interface Demotable 
	satisfies Rankable {

	shared default void demote() => promoTitles = (promoTitles.size>0 then promoTitles.spanTo(promoTitles.size-2) else []);

}