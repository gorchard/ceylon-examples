
shared interface Promotable
	satisfies Rankable {

	shared default void promote() => promoTitles = SequenceBuilder<String>().appendAll(promoTitles).append("friend").sequence;
	
	shared formal Boolean wasPromoted();

}