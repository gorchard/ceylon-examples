

"Proper manners are important in high society"
shared class RegalCourtesy(String salutation, String? ancestry, String name = "Stranger", String|NobilityTitleEnum* titles)
	extends Courtesy(salutation, name, *[]) {
	
	
	shared actual Boolean vip = true;
	
	"Creates proper greeting (overwritten)"
	shared actual String greet() {

		// collect nobility titles
		variable List<NobilityTitleEnum> nobilityTitles = LazyList([]);
		
		// set string titles
		for(title in titles) {
			switch(title)
			case(is String) {
				promoTitles = SequenceBuilder<String>().appendAll(promoTitles).append(title).sequence;
			} 
			case(is NobilityTitleEnum) {
				nobilityTitles = LazyList({ title, *nobilityTitles});
				// or nobilityTitles = LazyList(concatenate({ *nobilityTitles }, {title}));
			}
		}
		
		String nobilityGreeting = ", ".join { for (nt in nobilityTitles) nt.address() };
		
		return super.greet() + " " + nobilityGreeting + " of " + (ancestry else "the abyss");
	}
	
}