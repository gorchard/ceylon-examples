import com.pirateninjaunicorn.ceylon.greetings { ... }

		// same as:
// String optionalArgs(String|Null name) {
String optionalArgs(String? name) { 
	// name might be NULL, so we are forced to check this:
	if (exists name) {
		return "Good Day, ``name``!";
	}
	else {
		return "Good Day, World!";
	}
}

String shortGreeting() {
	return "Hello, ``process.arguments.first else "World"``!";
}

String mandatoryArgs(String name) {
	// actually we're not allowed to check 'exists' here
	// but yeah, we got multiline text ;)
	return "This is ``name`` ``language.version`` 
           running on Java ``runtime.version``!\n
           You ran me at ``system.milliseconds`` ms,
           with ``process.arguments.size`` arguments.";
}

// same as:
// String? additional() {
String|Null additional() {
	return process.arguments.size>0 then "cowboy";
}

doc ("The classic Hello World program")
by ("me")
void main() {
  
	String greeting = optionalArgs(process.arguments.first);
	print(greeting);
    
	// note: you can't name method and argument the same
	String shortGreetingVal = shortGreeting();
	print("just sayin': " + shortGreetingVal);
    
	variable String message = mandatoryArgs("Ceylon");
	print(message);
	// Beware of immutable awesomeness: we can only reassign
	// values with annotation "variable" (these are annotations!)
	message = "Nothing to say.";
    
	String goodBye = "So long, " + (additional() else "fellas") + "!";
	print(goodBye);
	print("");
    
    
	/***************************************************
	Good Day, test!
	just sayin': Hello, test!
              This is Ceylon 1.0.0 
              running on Java 1.7!
              
              You ran me at 1384551827428 ms,
              with 1 arguments.
              So long, cowboy!
	***************************************************/
    
	// let's try some courtesy...
	String[] titles = ["important", "foolish", "plain"];
	variable Courtesy x = Courtesy("Dear", "commoner", *titles);
	print(x.greet());
	x.demote();
	x.demote();
	print(x.greet());
	x.promote();
	print(x.greet());
	x.demote();
	print(x.greet());
	print("");
    
	/***************************************************
	Dear important foolish plain commoner
	Dear important commoner
	Dear important baron commoner
	Dear important commoner
	***************************************************/
    
	// whooo, we're at a royal party...
	x.printInvitation();
	x.promote();
	x.printInvitation();
    
	String[] regalTitles = ["Lord"];
	variable RegalCourtesy regal = RegalCourtesy("Dear", "Devon", "Richard", *regalTitles);
	regal.printInvitation();
	
	String[] regalTitles2 = ["Lady"];
	variable RegalCourtesy regal2 = RegalCourtesy("Dear", null, "Joselyn", *regalTitles2);
	regal2.printInvitation();
    
	/***************************************************
	+--------------------------------------------------+
	| Dear important commoner,                         |
	| we would be delighted to                         |
	| welcome you to our party.                        |
	+--------------------------------------------------+
	
	+--------------------------------------------------+
	| Dear important friend commoner,                  |
	| we would be delighted to                         |
	| welcome you to our party.                        |
	| Please take the VIP entrance.                    |
	+--------------------------------------------------+
	
	+--------------------------------------------------+
	| Dear Lord Richard  of Devon,                     |
	| we would be delighted to                         |
	| welcome you to our party.                        |
	| Please take the VIP entrance.                    |
	+--------------------------------------------------+
	
	+--------------------------------------------------+
	| Dear Lady Joselyn  of the abyss,                 |
	| we would be delighted to                         |
	| welcome you to our party.                        |
	| Please take the VIP entrance.                    |
	+--------------------------------------------------+
	***************************************************/
	
	// adding some static-type-magic
	Greeting someGreeting = Ignorance();
	someGreeting.printInvitation();
	
	// this is type check and assignment in one step: Like!
	if(is Ignorance someGreeting) {
		print(someGreeting.greet() + " I am ignorant ;)");
	}
	if(is Promotable someGreeting) {
		print("... and I am promotable");
	}
	
	// or we can use the switch:
	/*
	switch(someGreeting)
	case (is Promotable) {
	 	print("... and I am promotable");
	}
	case (is Ignorance) {
	 	print(someGreeting.greet() + " I am ignorant ;)");
	}
	else {
	 	print(someGreeting.greet() + " I am unsure");
	}
	*/
	print("");
	
	/***************************************************
	+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
	/ What's up, dude!,                     /
	\ move yer sorry ass to                 \
	/ da party tha night.                   /
	+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
	
	What's up, dude! I am ignorant ;)
	***************************************************/
	
	// intersection type example
	Promotable&Demotable officerGreeting = Courtesy("Good afternoon", *["Officer"]);
	
	// union type example
	variable Promotable|Ignorance test = officerGreeting;
	test = regal2;
	
	Demotable demotable = Ignorance();
	// test = demotable; // fails!
	if(is Ignorance demotable) {
		test = demotable; // works!
	}
	
	// royal again
	NobilityTitleEnum duchess = duke;
	{<String|NobilityTitleEnum>+} someTitles = {"Wench", duchess};
	// or short:
	// value someTitles = {"Wench", duchess};
	
	Courtesy courtesy = RegalCourtesy("Good evening", "Tavern", "Mable", *someTitles);
	courtesy.printInvitation();
	value title = duchess.title;
	NobilityTitleEnum? nobilityTitle = nobilityTitleEnumFromValue(title);
	print(nobilityTitle);
	
	print("");
	
	/***************************************************
	+--------------------------------------------------+
	| Good evening Wench Mable Your Grace of Tavern,   |
	| we would be delighted to                         |
	| welcome you to our party.                        |
	| Please take the VIP entrance.                    |
	+--------------------------------------------------+
	 
	Duke
	***************************************************/
	
}